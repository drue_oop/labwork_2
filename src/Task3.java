public class Task3 {
	public static int findMax(int ...values) {
		int max = 0;
		for(int val : values) if(val > max) max = val;
		return max;
	}

	public static int findMin(int ...values) {
		int min = (int) Double.POSITIVE_INFINITY;
		for(int val : values) if(val < min) min = val;
		return min;
	}

	public static int findAverage(int ...values) {
		int average = 0;
		for(int val : values) average += val;
		return average / values.length;
	}
}