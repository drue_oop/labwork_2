public class Task4 {
	public static int findDoubleFactorial(int number) {
		int result = 1;
		for(int i = number; i > 0; i -= 2) result *= i;
		return result;
	}
	public static int findDoubleFactorialRecursion(int number) {
		int result = (number > 0) ? number : 1;
		if(number - 2 > 0) result *= findDoubleFactorialRecursion(number - 2);
		return result;
	}
}