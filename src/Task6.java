public class Task6 {
	public static int[] getSubArray(int[] arr, int length) {
		int arr_size = Math.min(arr.length, length);
		int[] result = new int[arr_size];
		for(int i = 0; i < arr_size; i++) result[i] = arr[i];
		return result;
	}
}