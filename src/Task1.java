public class Task1 {
	private char symbol;
	private String string;


	public void setValue(char val) {
		symbol = val;
	}
	public void setValue(String val) {
		string = val;
	}
	public void setValue(char[] val) {
		if(val.length == 1) {
			symbol = val[0];
		} else if(val.length > 1) {
			string = "";
			for(char item : val) string += item;
		}
	}

	public void showValues() {
		System.out.println("Symbol = " + symbol);
		System.out.println("String = " + string);
	}
}