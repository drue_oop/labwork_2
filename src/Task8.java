public class Task8 {
	public static int getAverageArray(int[] arr) {
		int average = 0;
		for(int i = 0; i < arr.length; i++) average += arr[i];
		return average / arr.length;
	}
}