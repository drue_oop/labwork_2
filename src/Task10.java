public class Task10 {
	public static int[] findMinMax(int ...values) {
		int min = (int) Double.POSITIVE_INFINITY;
		int max = 0;
		for(int val : values) {
			if(val < min) min = val;
			if(val > max) max = val;
		}
		return new int[]{min, max};
	}
}