public class Task7 {
	public static int[] charsToInts(char[] arr) {
		int[] result = new int[arr.length];
		for(int i = 0; i < arr.length; i++) result[i] = arr[i];
		return result;
	}
}