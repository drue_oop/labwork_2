public class Task5 {
	public static int sumOfSquares(int number) {
		int result = 0;
		for(int i = number; i > 0; i--) result += i * i;
		return result;
	}
	public static int sumOfSquaresRecursion(int number) {
		int result = number * number;
		if(number - 1 > 0) result += sumOfSquaresRecursion(number - 1);
		return result;
	}
}