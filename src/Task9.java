public class Task9 {
	public static void reverseArray(char[] arr) {
		for(int i = 0, j = arr.length - 1; i < j; i++, j--) {
			char temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
	}
}